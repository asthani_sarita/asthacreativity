# asthacreativity

Name : Sarita Asthani
District : Lalitpur
Country : Nepal

Asthacreativity is the name I created to show my works I've done so far.
Here, I present you my portfolio with regards to the field of graphic design, illustration and ui.
I have completed my bachelor's degree in "Bsc(Hons.) in Computing" and yet I am self learning.
I am eager to know the design process that comes ahead.

Resources
---------
HTML
CSS
Bootstrap 4
Font_awesome
JavaScript


Fonts Name Used
---------------
'EB Garamond', 
'Quicksand'


Sources of Images
------------------
All the images are my pieces of work and arts.
No copyright distributed.





